<?php
/**
 * Created by PhpStorm.
 * User: abdirahim
 * Date: 13/04/2018
 * Time: 00:50
 */
return [
'doc'  => array('application/msword', 'application/vnd.ms-office'),
    'docx' => array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip')
    ];