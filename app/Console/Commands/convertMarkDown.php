<?php

namespace App\Console\Commands;

use App\User;

use Illuminate\Console\Command;

use Michelf\MarkdownExtra;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;


class convertMarkDown extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'markdown:convert {file?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert a Markdown file to HTML';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // or just pass the timezone as a string
        $nowInLondonTz = Carbon::now('Europe/London');
        // Get the currently authenticated user...
        $user = Auth::user();
        // Log user
        $this->logEvent($user.' is converting the file.'.$nowInLondonTz);

        $file = public_path('files/markdown.md');

        // validate file

        $markdown = file_get_contents($file);
        $markdownParser = new MarkdownExtra();
        $content = $markdownParser->transform($markdown);

        $html = "<html>
                <head>
                </head>
                <body>$content</body>
                </html>";

        $this->saveHTML($html);

    }

    public function logEvent($message) {

//        Storage::disk('s3')->put('log.txt', $message)
//        Storage::disk('sftp')->put('log.txt', $message);
        Storage::disk('local')->append('log.txt', $message);

    }

    public function saveHTML($html) {

        Storage::disk('local')->put('htmlFile.html', $html);

    }
}
